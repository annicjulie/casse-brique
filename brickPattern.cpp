#include <iostream>
#include <fstream>
#include <map>

#include "brickPattern.h"
#include "brick.h"
#include "point.h"
#include "brickBreakable.h"
#include "libraries/pugixml.hpp"
#include "standartSurface.h"
#include "killingSurface.h"


using namespace pugi;
using namespace std;


brickPattern::brickPattern(string patternFilePath):d_patternFilePath{patternFilePath}{
    readFile();
}

void brickPattern::readFile(){


    ifstream pattern_file(d_patternFilePath);
    xml_document doc;
    doc.load(pattern_file);

    auto bricks = doc.child("pattern").children();

    int col=0;
    int line=0;

    for(auto&& b: bricks){

        map<string, string> brickParam;

        string brickT = b.name();
        int brickX = BRICK_WIDTH*col;
        int brickY = BRICK_HEIGHT*line;


        for(xml_attribute attr : b.attributes()){
            brickParam[attr.name()]=attr.value();
        }


        auto surfaces=b.children();
        string surfaceT;
        map<string, string> surfaceParam;

        for(auto&& s: surfaces){
            surfaceT=s.name();
            for(xml_attribute attr : s.attributes()){
                surfaceParam[attr.name()]=attr.value();
            }
        }


        brick* brck=addBrick(brickT,brickX,brickY,createSurface(surfaceT,surfaceParam),brickParam);

        if(surfaceT=="killingSurface"){
            brck->setIsKiller(true);
        }

        d_pattern.push_back(brck);


        col++;

        if(col==NB_COLUMN){
            col=0;
            line++;
        }

    }


}

string brickPattern::getPatternFilePath(){
    return d_patternFilePath;
}


vector<brick*> brickPattern::getPattern(){
    return d_pattern;
}

void brickPattern::setPattern(vector<brick*> pattern){
    d_pattern=pattern;
}


int brickPattern::getPatternSize() const{
    return d_pattern.size();
}

brick* brickPattern::getBrick(int indice) const{
    return d_pattern[indice];
}

vector<brick*> brickPattern::deleteDestroyedBrick(){

    for(int i=0;i<d_pattern.size();i++){

        if(d_pattern[i]->isDestroyed()==true){

            d_pattern[i]=nullptr;
        }
    }

    return d_pattern;

}

bool brickPattern::isEmpty(){
    bool empt=true;
    int i=0;

    while(empt==true && i<d_pattern.size()){
        if(d_pattern[i]!=nullptr){
            if(d_pattern[i]->getState()!=-1){
                empt=false;
            }

        }

        i++;
    }

    return empt;
}


brick* brickPattern::addBrick(string type, int x, int y, surface* surface, map<string,string> param){

    if(surface!=nullptr){

        if(type=="brickBreakable"){

            brick* br=new brickBreakable(point{x+TOP_LEFT_CORNER_PATTERN_AREA.getX(),y+TOP_LEFT_CORNER_PATTERN_AREA.getY()}, point{x+BRICK_WIDTH+TOP_LEFT_CORNER_PATTERN_AREA.getX(),y+BRICK_HEIGHT+TOP_LEFT_CORNER_PATTERN_AREA.getY()}, stoi(param["state"]));
            surface->setObjetSurfacique(br);
            return br;
        }else if(type=="brick"){
            brick* br=new brick(point{x+TOP_LEFT_CORNER_PATTERN_AREA.getX(),y+TOP_LEFT_CORNER_PATTERN_AREA.getY()}, point{x+BRICK_WIDTH+TOP_LEFT_CORNER_PATTERN_AREA.getX(),y+BRICK_HEIGHT+TOP_LEFT_CORNER_PATTERN_AREA.getY()});
            surface->setObjetSurfacique(br);
            return br;
        }else{

            return nullptr;

        }

    }else{
        return nullptr;


    }



}

surface* brickPattern::createSurface(string type, map<string,string> param){

    if(type=="standardSurface"){

        return new standartSurface(stof(param["speedCoef"]));

    }else if(type=="killingSurface"){

       return new killingSurface();

    }else{

        return nullptr;
    }

}


