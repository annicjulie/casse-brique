#ifndef BALL_H
#define BALL_H

#include "point.h"

class ball {

public:

    static const int RADIUS = 5;

    ball();
    ball(point &center, float speed, float direction);
    ball(int x, int y, float speed, float direction);
    float getSpeed() const;
    void setSpeed(const float &speed);
    float getDirection() const;
    void setDirection(const float &dir);
    point getCenter() const;
    void setCenter(const point &p);
    void moveb();

private:
    point d_center;
    float d_speed, d_direction;
};

#endif // BALL_H
