#ifndef KILLINGSURFACE_H
#define KILLINGSURFACE_H

#include "surface.h"
#include "brick.h"


class killingSurface : public surface
{
    public:
        killingSurface(objetSurfacique *obj);
        killingSurface();

        virtual ~killingSurface();

        virtual bool touchTopOrBottomSide() const override;
        virtual void changeBall(ball& ball, bool& ballKilled) const override;
        virtual point collisionPoint(const ball& ball,bool &touched) override;

        virtual void touched(ball& ball) override;
        virtual void setObjetSurfacique(objetSurfacique *obj) override;

    protected:

    private:
        objetSurfacique *d_obj;

};

#endif // KILLINGSURFACE_H
