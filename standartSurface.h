#ifndef STANDARTSURFACE_H
#define STANDARTSURFACE_H

#include "surface.h"

class standartSurface : public surface
{
    public:
        standartSurface(float speedCoef,objetSurfacique *obj);
        standartSurface(float speedCoef);

        virtual ~standartSurface();
  
        virtual bool touchTopOrBottomSide() const override;
        virtual void changeBall(ball& ball, bool& ballKilled) const override;
        
        virtual point collisionPoint(const ball& ball,bool &touched) override;

        virtual void touched(ball& ball) override;
        virtual void setObjetSurfacique(objetSurfacique *obj) override;
        objetSurfacique* getObjetSurfacique();

        float getSpeedCoef() const;

    protected:

    private:
        objetSurfacique *d_obj;
        float d_speedCoef;

};

#endif // STANDARTSURFACE_H
