#ifndef BRICKPATTERN_H
#define BRICKPATTERN_H

#include"point.h"
#include"brick.h"
#include<vector>
#include<string>
#include<memory>
#include <map>

using namespace std;


class brickPattern{

public:

    brickPattern(string patternFilePath);

    void readFile();

    string getPatternFilePath();

    vector<brick*> getPattern();
    void setPattern(vector<brick*> pattern);

    int getPatternSize() const;
    brick* getBrick(int indice) const;

    vector<brick*> deleteDestroyedBrick();
    bool isEmpty();


private:

    string d_patternFilePath;
    vector<brick*> d_pattern;

    brick* addBrick(string type, int x, int y, surface* surface, map<string,string> param);
    surface* createSurface(string type, map<string,string> param);


};

static const int BRICK_WIDTH=80;
static const int BRICK_HEIGHT=50;
static const int NB_COLUMN=15;
static const point TOP_LEFT_CORNER_PATTERN_AREA{350,50};



#endif // BRICKPATTERN_H

