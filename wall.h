#ifndef WALL_H
#define WALL_H

#include <iostream>
#include "point.h"
#include "standartSurface.h"
#include "objetSurfacique.h"

class wall : public objetSurfacique
{
    public:
        wall();
        wall(const point &TopLeft, const point& BottomRight);
        virtual ~wall();

        virtual point getCornerTopLeft() const override;
        virtual point getCornerBottomRight() const override;

        void setCornerTopLeft(const point& p);
        void setCornerTopLeft(int x, int y);
        void setCornerBottomRight(const point& p);
        void setCornerBottomRight(int x, int y);
        standartSurface getSurface();

    private:
        point d_cornerTopLeft;
        point d_cornerBottomRight;
        standartSurface d_wallSurface;

};

#endif // WALL_H

