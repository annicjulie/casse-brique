#ifndef BRICKBREAKABLE_H
#define BRICKBREAKABLE_H


#include"brick.h"
#include"surface.h"


class brickBreakable : public brick{

public:

    brickBreakable(point cornerTopLeft, point cornerBottomRight, int state);
    brickBreakable(point cornerTopLeft, point cornerBottomRight, int state, surface *surface);


    virtual int getState() const override;
    virtual void setState(int state) override;

    virtual void decreaseState() override;
    virtual bool isDestroyed() const override;


private:

    int d_state;



};

#endif // BRICKBREAKABLE_H
