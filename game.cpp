#include "game.h"

game::game(string mapPath): d_gameOver{false}, d_brickPattern{brickPattern(mapPath)}, d_graphicalInterface{graphicalInterface()}
{
    point centreBall(960,940);
    d_ball = ball(centreBall,10,45);

    point topLeftRacket(910,950);
    d_racket = new racket(topLeftRacket);
    d_racket->getSurface().setObjetSurfacique(d_racket);

    point topLeftWallTop(0,0);
    point bottomRigthWallTop(1920,25);
    d_topWall = new wall(topLeftWallTop,bottomRigthWallTop);

    point topLeftWallLeft(0,25);
    point bottomRigthWallLeft(275,1000);
    d_leftWall = new wall(topLeftWallLeft,bottomRigthWallLeft);

    point topLeftWallRight(1620,0);
    point bottomRigthWallRight(1920,1005);
    d_rightWall = new wall(topLeftWallRight,bottomRigthWallRight);
}

void game::run()
{
    initInterface();
    while(!d_gameOver)
    {
        d_graphicalInterface.eraseRacket(*d_racket);
        /*if(GetAsyncKeyState(VK_LEFT))
                d_racket.moveLeft();
        if(GetAsyncKeyState(VK_RIGHT))
                d_racket.moveRight();*/
        d_graphicalInterface.showRacket(*d_racket);

        d_graphicalInterface.eraseBall(d_ball);
        d_ball.moveb();
        d_racket->getSurface().touched(d_ball);
        cout << "3";
        d_graphicalInterface.showBall(d_ball);
        delay(200);
    }
}

void game::initInterface()
{
    d_graphicalInterface.showWall(*d_topWall);
    d_graphicalInterface.showWall(*d_leftWall);
    d_graphicalInterface.showWall(*d_rightWall);
    d_graphicalInterface.showRacket(*d_racket);
    d_graphicalInterface.showBrickPattern(d_brickPattern);
    d_graphicalInterface.showBall(d_ball);
}

bool game::getGameOver()
{
    return d_gameOver;
}
void game::setGameOver(bool state)
{
    d_gameOver =  state;
}
