#include "point.h"

point::point() : d_x{0}, d_y{0} {}

point::point(int x, int y) : d_x{x}, d_y{y} {}

int point::getX() const {return d_x;}

void point::setX(int x) {d_x = x;}

int point::getY() const {return d_y;}

void point::setY(int y) {d_y = y;}

void point::setXY(int x, int y) {d_x = x; d_y = y;}
