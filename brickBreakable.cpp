#include "brick.h"
#include "point.h"
#include "brickBreakable.h"
#include "surface.h"

brickBreakable::brickBreakable(point cornerTopLeft, point cornerBottomRight, int state): brick{cornerTopLeft, cornerBottomRight, nullptr}, d_state{state}
{}

brickBreakable::brickBreakable(point cornerTopLeft, point cornerBottomRight, int state, surface* surface): brick{cornerTopLeft, cornerBottomRight, surface}, d_state{state}
{}


int brickBreakable::getState() const {return d_state;}

void brickBreakable::setState(int state){
    d_state=state;
}

void brickBreakable::decreaseState(){
    d_state--;
}

bool brickBreakable::isDestroyed() const{
    return d_state==0;
}
