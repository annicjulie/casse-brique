#include "graphicalInterface.h"
#include "windows.h"
#include <iostream>

graphicalInterface::graphicalInterface(void)
{
    opengraphsize(WINDOW_WIDTH,WINDOW_HEIGHT); //Ouverture de la fenetre graphique
}

void graphicalInterface::showBall(const ball &b)
{
    drawBall(b, WHITE);
}

void graphicalInterface::showBrickPattern(const brickPattern &bp)
{
    for(int i=0; i<bp.getPatternSize(); ++i)
    {
        if(bp.getBrick(i) != nullptr)
            showBrick(*bp.getBrick(i));
    }
}

void graphicalInterface::showRacket(const racket &r)
{
    drawRacket(r, GREEN);
}

void graphicalInterface::showWall(const wall &w)
{
    drawWall(w, LIGHTGRAY);
}

void graphicalInterface::showBrick(const brick &b)
{
    setlinestyle(0,0,1);
    int etat = b.getState();

    int COULEUR;
    switch(etat) {
        //Surface incassable -> grise, surface tueuse(obligatoirement incassable) -> rouge
        case -1: if(b.getIsKiller()){COULEUR = RED;}else{COULEUR = WHITE;}; break;

        //Différente couleurs des briques cassable en fonction de leur état
        case 1: COULEUR = etat; break;
        case 2: COULEUR = etat; break;
        case 3: COULEUR = etat; break;
        case 4: COULEUR = etat; break;
        case 5: COULEUR = etat+1; break;
        case 6: COULEUR = etat+1; break;
        case 7: COULEUR = etat+1; break;
        case 8: COULEUR = etat+1; break;
        case 9: COULEUR = etat+1; break;
        case 10: COULEUR = etat+1; break;
    }
    drawBrick(b,COULEUR);
}

void graphicalInterface::eraseBall(const ball &b)
{
    drawBall(b, BLACK);
}

void graphicalInterface::eraseBrick(const brick &b)
{
    drawBrick(b, BLACK);
}

void graphicalInterface::eraseWall(const wall &w)
{
    drawWall(w, BLACK);
}

void graphicalInterface::eraseRacket(const racket &r)
{
    drawRacket(r, BLACK);
}


////////////////////
///METHODES PRIVE///
////////////////////

void graphicalInterface::drawBall(const ball &b, int colorFill)
{
    setlinestyle(0,0,1); //Largeur de ligne à 1 pixels
    setcolor(colorFill);
    circle(b.getCenter().getX(), b.getCenter().getY(), ball::RADIUS);
    setfillstyle(SOLID_FILL,colorFill); //définition du remplissage plein
    setcolor(colorFill);
    floodfill(b.getCenter().getX(),b.getCenter().getY(),colorFill);
}

void graphicalInterface::drawBrick(const brick &b, int colorFill)
{
    setcolor(colorFill);
    bar(b.getCornerTopLeft().getX(),b.getCornerTopLeft().getY(),b.getCornerBottomRight().getX(),b.getCornerBottomRight().getY());
    setlinestyle(0,0,2);
    setcolor(BLACK);
    rectangle(b.getCornerTopLeft().getX(),b.getCornerTopLeft().getY(),b.getCornerBottomRight().getX(),b.getCornerBottomRight().getY());
}

void graphicalInterface::drawWall(const wall &w, int colorFill)
{
    setcolor(colorFill);
    bar(w.getCornerTopLeft().getX(),w.getCornerTopLeft().getY(),w.getCornerBottomRight().getX(),w.getCornerBottomRight().getY());
}

void graphicalInterface::drawRacket(const racket &r, int colorFill)
{
    setcolor(colorFill);
    bar(r.getCornerTopLeft().getX(),r.getCornerTopLeft().getY(),r.getCornerBottomRight().getX(),r.getCornerBottomRight().getY());
}
