#include "standartSurface.h"

standartSurface::standartSurface(float speedCoef,objetSurfacique *obj) : d_obj{obj}, d_speedCoef{speedCoef}
{}

standartSurface::standartSurface(float speedCoef) : d_obj{nullptr}, d_speedCoef{speedCoef}
{}

standartSurface::~standartSurface()
{}

bool standartSurface::touchTopOrBottomSide() const
{
    if (d_obj->returnHeight() > d_obj->returnLength())
		return true;
	else
		return false;
}




void standartSurface::changeBall(ball& ball, bool& ballKilled) const
{
    ball.setSpeed( ball.getSpeed() * d_speedCoef );
    ballKilled = false;
}

point standartSurface::collisionPoint(const ball& ball,bool &touched)
{
    point pointCol{};
	point center{ball.getCenter().getX(),ball.getCenter().getY()};
	int xCenter = center.getX(), yCenter = center.getY();
	touched = true;

    if(xCenter + ball::RADIUS >= d_obj->topLeftCoordsX())
	{
		if(yCenter - ball::RADIUS <= d_obj->bottomRightCoordsY())
			point pointCol{xCenter + ball::RADIUS, yCenter - ball::RADIUS}; //bottomLeft corner
		else if (yCenter + ball::RADIUS == d_obj->topLeftCoordsY())
			point pointCol{xCenter + ball::RADIUS, yCenter + ball::RADIUS}; //topLeft corner
		else
			point pointCol{xCenter+ ball::RADIUS ,yCenter}; //leftSide
	}

	else if( xCenter - ball::RADIUS <= d_obj->bottomRightCoordsX() )
	{
		if(yCenter - ball::RADIUS == d_obj->bottomRightCoordsY())
			point pointCol{xCenter - ball::RADIUS, yCenter - ball::RADIUS}; //bottomRight corner
		else if (yCenter + ball.RADIUS == d_obj->topLeftCoordsY())
			point pointCol{xCenter - ball::RADIUS, yCenter + ball::RADIUS}; //topRight corner
		else
			point pointCol{xCenter - ball::RADIUS,yCenter}; //rightSide
	}

	else if( yCenter - ball::RADIUS <= d_obj->bottomRightCoordsY())
		point pointCol{xCenter,yCenter - ball::RADIUS}; //bottomSide

	else if(yCenter + ball::RADIUS >= d_obj->topLeftCoordsY())
		point pointCol{xCenter,yCenter + ball::RADIUS}; //topSide
    else
    {
        point pointCol{-1,-1};
        touched = false;
    }
    return pointCol;

}

void standartSurface::touched(ball& ball)
{
    bool touched,killed;
    point collision{};
    collision.setXY( collisionPoint(ball, touched).getX(),collisionPoint(ball, touched).getY());


    if(touched)
    {
        changeBall(ball,killed);
        if(!killed)
        {   //For WALL and BRICK

            if(d_obj->bottomRightCoordsY() == 950 )
            {
                if(collision.getX() <= d_obj->topLeftCoordsX() + (d_obj->returnLength()/2) ) //if the ball touch the left part of the racket's top side
					ball.setDirection(135);
				else
					ball.setDirection(90); //if the ball touch the right part of the racket's top side
            }

            else if(ball.getDirection() == 45) //only bottom and left side
            {
                if(collision.getY() == d_obj->bottomRightCoordsY() ) //if touch bottom side
                    substractBallAngle(ball);
                else if(collision.getX() == d_obj->topLeftCoordsX() ) //if touch left side
                    addBallAngle(ball);
            }

            else if (ball.getDirection() == 135) //bottom and right side
            {
                if(collision.getY() == d_obj->bottomRightCoordsY()) //if touch bottom side
                    addBallAngle(ball);
                else if(collision.getX() == d_obj->bottomRightCoordsX()) //if touch right side
                    substractBallAngle(ball);
            }

            else if (ball.getDirection() == 225) //top and right side
            {
                if (collision.getY() == d_obj->topLeftCoordsY() ) //if touch top side
                    substractBallAngle(ball);
                else if( collision.getX() == d_obj->bottomRightCoordsX() ) //if touch right side
                    addBallAngle(ball);
            }

            else if (ball.getDirection() == 315) //top and left side
            {
                if (collision.getY() == d_obj->topLeftCoordsY() ) //if touch top side
                    addBallAngle(ball);
                else if( collision.getX() == d_obj->topLeftCoordsX() ) //if touch right side
                    substractBallAngle(ball);
            }
        }
    }
}


objetSurfacique* standartSurface::getObjetSurfacique()
{
    return d_obj;
}


void standartSurface::setObjetSurfacique(objetSurfacique *obj){
    d_obj=obj;
}

float standartSurface::getSpeedCoef() const
{
    return d_speedCoef;
}


