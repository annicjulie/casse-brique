#ifndef GRAPHICALINTERFACE_H
#define GRAPHICALINTERFACE

#include "libraries\graphics.h"
#include "ball.h"
#include "brickBreakable.h"
#include "wall.h"
#include "brickPattern.h"
#include "racket.h"

class graphicalInterface {
public:
    graphicalInterface(void);

    void showBall(const ball &b);
    void showBrick(const brick &b);
    void showBrickPattern(const brickPattern &bp);
    void showWall(const wall &w);
    void showRacket(const racket &r);

    void eraseBall(const ball &b);
    void eraseBrick(const brick &b);
    void eraseWall(const wall &w);
    void eraseRacket(const racket &r);

private:
    const int WINDOW_WIDTH = 1920;
    const int WINDOW_HEIGHT = 1080;

    void drawBall(const ball &b, int colorFill);
    void drawBrick(const brick &b, int colorFill);
    void drawWall(const wall &w, int colorFill);
    void drawRacket(const racket &r, int colorFill);
};


#endif // GRAPHICALINTERFACE_H
