#ifndef OBJETSURFACIQUE_H
#define OBJETSURFACIQUE_H

#include "point.h"

class objetSurfacique
{
    public:
        virtual ~objetSurfacique();

        virtual point getCornerTopLeft() const = 0;
        virtual point getCornerBottomRight() const = 0;

        int returnHeight() const;
        int returnLength() const;


        int topLeftCoordsX() const;
        int topLeftCoordsY() const;
        int bottomRightCoordsX() const;
        int bottomRightCoordsY() const;




};

#endif // OBJETSURFACIQUE_H
