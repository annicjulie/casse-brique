#ifndef BRICK_H
#define BRICK_H

#include"point.h"
#include"objetSurfacique.h"
#include"surface.h"


class brick : public objetSurfacique{
public:

    brick(point cornerTopLeft, point cornerBottomRight);
    brick(point cornerTopLeft, point cornerBottomRight, surface *surface);


    virtual point getCornerTopLeft() const override;
    void setCornerTopLeft(point cornerTopLeft);
    point getCornerBottomRight() const;
    void setCornerBottomRight(point cornerBottomRight);
    surface* getSurface() const;
    void setSurface(surface* surface);

    bool getIsKiller() const;

    virtual int getState() const;
    virtual void setState(int state);

    virtual void decreaseState();
    virtual bool isDestroyed() const;



private:

    point d_cornerTopLeft;
    point d_cornerBottomRight;
    surface *d_surface;
    bool isKiller;

};



#endif // BRICK_H
