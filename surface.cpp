#include "surface.h"

surface::~surface()
{
    //dtor
}


void surface::addBallAngle(ball &ball) const
{
    if( ball.getDirection() == 315 )
		ball.setDirection(45);
	else
		ball.setDirection( ball.getDirection() + 90 );
}



void surface::substractBallAngle(ball &ball) const
{
    if( ball.getDirection() == 45 )
		ball.setDirection(315);
	else
		ball.setDirection( ball.getDirection() - 90 );
}
