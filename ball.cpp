#include "ball.h"
#include <cmath>

ball::ball() : d_center{}, d_speed{0.0}, d_direction{0.0}
{}

ball::ball(point &center, float speed, float direction) : d_center{center}, d_speed{speed}, d_direction{direction}
{}

ball::ball(int x, int y, float speed, float direction) : d_center{x,y}, d_speed{speed}, d_direction{direction}
{}

float ball::getSpeed() const
{
    return d_speed;
}

void ball::setSpeed(const float &speed)
{
    d_speed=speed;
}

float ball::getDirection() const
{
    return d_direction;
}

void ball::setDirection(const float &dir)
{
    d_direction=dir;
}

point ball::getCenter() const
{
    return d_center;
}

void ball::setCenter(const point &p)
{
    d_center.setXY(p.getX(),p.getY());
}

void ball::moveb()
{
    double sinus = sin(d_direction*(M_PI/180));
    double cosinus = cos(d_direction*(M_PI/180));

    int newX = static_cast<int>(d_center.getX() + (d_speed*cosinus));
    int newY = static_cast<int>(d_center.getY() - (d_speed*sinus));

    d_center.setX(newX);
    d_center.setY(newY);
}
