#ifndef RACKET_H
#define RACKET_H

#include "point.h"
#include "standartSurface.h"
#include "objetSurfacique.h"

class racket : public objetSurfacique {

public:
    racket();
    racket(point &topLeft);
    racket(int x, int y);

    void setCornerTopLeft(const point &p);
    void setCornerTopLeft(int x, int y);

    virtual point getCornerTopLeft() const override;
    virtual point getCornerBottomRight() const override;

    standartSurface getSurface() const;

    void moveLeft();
    void moveRight();

    static const int WIDTH = 100;
    static const int HEIGHT = 20;
    static constexpr float SPEEDCOEF = 1.0;

private:
    point d_cornerTopLeft;
    standartSurface d_surface;
};

#endif // RACKET_H
