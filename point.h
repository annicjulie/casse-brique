#ifndef POINT_H
#define POINT_H

class point {
public:
    point();
    point(int x, int y);
    int getX() const;
    void setX(int x);
    int getY() const;
    void setY(int y);
    void setXY(int x, int y);
private:
    int d_x, d_y;
};

#endif // POINT_H
