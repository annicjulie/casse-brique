#include "killingSurface.h"

killingSurface::killingSurface(objetSurfacique *obj) : d_obj{obj}
{}

killingSurface::killingSurface() : d_obj{nullptr}
{}

killingSurface::~killingSurface()
{}

bool killingSurface::touchTopOrBottomSide() const
{
    if (d_obj->returnHeight() > d_obj->returnLength())
		return true;
	else
		return false;
}

void killingSurface::changeBall(ball& ball, bool& ballKilled) const
{
    ballKilled = true;
}

point killingSurface::collisionPoint(const ball& ball, bool& touched)
{
point pointCol{};
	point center = ball.getCenter();
	int xCenter = center.getX(), yCenter = center.getY();
	touched = true;

    if(xCenter + ball::RADIUS >= d_obj->topLeftCoordsX())
	{
		if(yCenter - ball::RADIUS <= d_obj->bottomRightCoordsY())
			point pointCol{xCenter + ball::RADIUS, yCenter - ball::RADIUS}; //bottomLeft corner
		else if (yCenter + ball::RADIUS == d_obj->topLeftCoordsY())
			point pointCol{xCenter + ball::RADIUS, yCenter + ball::RADIUS}; //topLeft corner
		else
			point pointCol{xCenter+ ball::RADIUS ,yCenter}; //leftSide
	}

	else if( xCenter - ball::RADIUS == d_obj->bottomRightCoordsX() )
	{
		if(yCenter - ball::RADIUS == d_obj->bottomRightCoordsY())
			point pointCol{xCenter - ball::RADIUS, yCenter - ball::RADIUS}; //bottomRight corner
		else if (yCenter + ball.RADIUS == d_obj->topLeftCoordsY())
			point pointCol{xCenter - ball::RADIUS, yCenter + ball::RADIUS}; //topRight corner
		else
			point pointCol{xCenter - ball::RADIUS,yCenter}; //rightSide
	}

	else if( yCenter - ball::RADIUS == d_obj->bottomRightCoordsY())
		point pointCol{xCenter,yCenter - ball::RADIUS}; //bottomSide

	else if(yCenter + ball::RADIUS == d_obj->topLeftCoordsY())
		point pointCol{xCenter,yCenter + ball::RADIUS}; //topSide
    else
    {
        point pointCol{-1,-1};
        touched = false;
    }
    return pointCol;
}

void killingSurface::touched(ball& ball)
{
    bool touched,killed;
    point p= collisionPoint(ball, touched);

    if(touched)
    {
        changeBall(ball,killed);
        //game.gameIsOver(true);
    }
}

void killingSurface::setObjetSurfacique(objetSurfacique *obj){
    d_obj=obj;
}
