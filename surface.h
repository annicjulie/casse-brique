#ifndef SURFACE_H
#define SURFACE_H

#include "ball.h"
#include "objetSurfacique.h"

class surface
{
    public:
        virtual ~surface();

        virtual void addBallAngle(ball& ball) const;
        virtual void substractBallAngle(ball &ball) const;
        virtual bool touchTopOrBottomSide() const = 0;
        
        virtual void changeBall(ball& ball, bool& ballKilled)const = 0;
        virtual point collisionPoint(const ball &ball,bool &touched) = 0;

        virtual void touched(ball &ball) =0;

        virtual objetSurfacique* getObjetSurfacique() = 0;
        virtual void setObjetSurfacique(objetSurfacique *obj) =0;


};

#endif // SURFACE_H
