#include "../point.h"
#include "../libraries/doctest.h"

TEST_CASE("Le constructeur de la classe point fonctionne correctement") {
    point p{};
    SUBCASE("Le point possède les coordonnées x=0 et y=0") {
        REQUIRE_EQ(p.getX(), 0);
        REQUIRE_EQ(p.getY(), 0);
    }
    SUBCASE("La fonction setX fonctionne") {
        p.setX(3);
        REQUIRE_EQ(p.getX(), 3);
    }
    SUBCASE("La fonction setY fonctionne") {
        p.setY(6);
        REQUIRE_EQ(p.getY(), 6);
    }
    SUBCASE("La fonction setXY fonctionne") {
        p.setXY(4,7);
        REQUIRE_EQ(p.getX(), 4);
        REQUIRE_EQ(p.getY(), 7);
    }
}

TEST_CASE("Le constructeur avec les paramètres x et y fonctionne correctement")  {
    point p{1,2};
    SUBCASE("Le point possède les coordonnées renseignées") {
        REQUIRE_EQ(p.getX(), 1);
        REQUIRE_EQ(p.getY(), 2);
    }
    SUBCASE("La fonction setX fonctionne") {
        p.setX(3);
        REQUIRE_EQ(p.getX(), 3);
    }
    SUBCASE("La fonction setY fonctionne") {
        p.setY(6);
        REQUIRE_EQ(p.getY(), 6);
    }
    SUBCASE("La fonction setXY fonctionne") {
        p.setXY(4,7);
        REQUIRE_EQ(p.getX(), 4);
        REQUIRE_EQ(p.getY(), 7);
    }
}

