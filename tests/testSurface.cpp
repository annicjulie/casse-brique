#include "../libraries/doctest.h"
#include "../wall.h"
#include "../point.h"
#include "../standartSurface.h"

inline void SpeedCoefEstCorrect(standartSurface s, float speedCoef)
{
    REQUIRE_EQ(s.getSpeedCoef(), speedCoef);
}

inline void ObjetSurfaciqueEstCorrect(standartSurface s, objetSurfacique* obj)
{
    REQUIRE_EQ(s.getObjetSurfacique(), obj);
}

inline void TopOrBottomEstCorrect(standartSurface s, bool booleen)
{
    REQUIRE_EQ(s.touchTopOrBottomSide(), booleen);
}

inline void AddBallAngleEstCorrect(standartSurface s, ball& b)
{
   REQUIRE_EQ(b.getDirection() , 45+90 );
}

inline void SubstractBallAngleEstCorrect(standartSurface s, ball& b)
{
    REQUIRE_EQ(b.getDirection(), 135-90);
}

inline void ChangeBallEstCorrect(ball& b, float speedCoef,float ballSpeed, bool killed)
{
    REQUIRE_EQ(b.getSpeed() , speedCoef * ballSpeed );
    REQUIRE_EQ(killed, false);
}

TEST_CASE("Les constructeurs de la classe standartSurface sont corrects")
{
    SUBCASE("Le constructeur avec un float pour le coefficient de speed fonctionne correctement")
    {
        float speed = 1.0;
        standartSurface s{ 1.0 };
        SpeedCoefEstCorrect(s, speed);  
    }
    SUBCASE("Le constructeur avec un float et un objet fonctionne correctement")
    {
        float speed = 1.0;
        wall* w = new wall{};
        standartSurface s{ 1.0,w };
        SpeedCoefEstCorrect(s, speed);
        ObjetSurfaciqueEstCorrect(s, w);
    }
}


TEST_CASE("Les fonctions de la classe standartSurface fonctionnent correctement")
{
    SUBCASE("La fonction setObjetSurfacique fonctionne correctement")
    {
        float speed = 1.0;
        wall* w = new wall{};
        standartSurface s{ 1.0};
        s.setObjetSurfacique(w);
        ObjetSurfaciqueEstCorrect(s,w);
    }
    SUBCASE("La fonction touchTopOrBottomSide fonctionne correctement")
    {
        float speed = 1.0;
        bool booleen = true;
        point p{ 1,1 };
        point p2{ 3,10 };

        wall* w = new wall{p,p2};
        standartSurface s{ 1.0,w };
        
        TopOrBottomEstCorrect(s, booleen );
    }

    SUBCASE("La fonction addBallAngle fonctionne correctement")
    {
        float speed = 1.0;
        ball b{};
        b.setDirection(45);
        point p{ 1,1 };
        point p2{ 2,10 };

        wall* w = new wall{ p,p2 };
        standartSurface s{ 1.0,w };
        s.addBallAngle(b);

        AddBallAngleEstCorrect(s, b);
    }

    SUBCASE("La fonction substractBallAngle fonctionne correctement")
    {
        float speed = 1.0;
        ball b{};
        b.setDirection(135);
        point p{ 1,1 };
        point p2{ 2,10 };

        wall* w = new wall{ p,p2 };
        standartSurface s{ 1.0,w };
        s.substractBallAngle(b);

        SubstractBallAngleEstCorrect(s, b);
    }

    SUBCASE("La fonction changeBall foncitonne correctement ")
    {
        float speed = 1.0;
        float ballSpeed = 2.0;
        point center{ 5,5 };

        ball b{};
        b.setDirection(135);
        b.setCenter(center);
        b.setSpeed(ballSpeed);
        
        point p{ 1,1 };
        point p2{ 2,10 };

        bool kill;

        wall* w = new wall{ p,p2 };
        standartSurface s{ 1.0,w };

        s.changeBall(b, kill);

        ChangeBallEstCorrect(b, speed, ballSpeed, kill);
    }
}
