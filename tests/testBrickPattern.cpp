
#include "../brickPattern.h"
#include "../libraries/doctest.h"

TEST_CASE("Le constructeur de la classe brickPattern fonctionne correctement") {
    string path="D:\\Ordinateur\\Documents\\MIAGE\\casse-brique\\patternFiles\\circles.xml";
    brickPattern bp{path};
    SUBCASE("La méthode getpatternfilepath fonctionne correctement") {
        REQUIRE_EQ(bp.getPatternFilePath(), path);
    }
    SUBCASE("Le vecteur a été correctement initialisé") {
        REQUIRE_EQ(bp.getPattern().size(), 150);
    }
}

