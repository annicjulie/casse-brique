#include "../brick.h"
#include "../brickBreakable.h"
#include "../standartSurface.h"
#include "../killingSurface.h"
#include "../libraries/doctest.h"

TEST_CASE("Le constructeur sans surface fonctionne correctement") {

    point p1{1,1};
    point p2{2,2};
    surface * s=new standartSurface(1);
    brickBreakable b{p1,p2,4};

    SUBCASE("La brick possède les bonnes coordonnées") {
        REQUIRE_EQ(p1.getX(), b.getCornerTopLeft().getX());
        REQUIRE_EQ(p1.getY(), b.getCornerTopLeft().getY());

        REQUIRE_EQ(p2.getX(), b.getCornerBottomRight().getX());
        REQUIRE_EQ(p2.getY(), b.getCornerBottomRight().getY());
    }
    SUBCASE("Les méthodes setCornerTopLeft et setCornerBottomRight fonctionnent correctement") {
        point p3{3,3};
        point p4{4,4};
        b.setCornerTopLeft(p3);
        b.setCornerBottomRight(p4);

        REQUIRE_EQ(p3.getX(), b.getCornerTopLeft().getX());
        REQUIRE_EQ(p3.getY(), b.getCornerTopLeft().getY());

        REQUIRE_EQ(p4.getX(), b.getCornerBottomRight().getX());
        REQUIRE_EQ(p4.getY(), b.getCornerBottomRight().getY());
    }
    SUBCASE("La méthode getState fonctionne correctement") {
        REQUIRE_EQ(b.getState(), 4);
    }
    SUBCASE("La méthode setState fonctionne correctement") {
        b.setState(8);
        REQUIRE_EQ(b.getState(), 8);
    }
    SUBCASE("La méthode decreaseState fonctionne correctement") {
        b.decreaseState();
        REQUIRE_EQ(b.getState(), 3);
    }
    SUBCASE("La méthode getSurface retorune nullptr") {
        REQUIRE_EQ(b.getSurface(), nullptr);
    }
    SUBCASE("La méthode setSurface retourne un pointeur sur sa surface") {
        b.setSurface(s);
        REQUIRE_EQ(b.getSurface(), s);
    }
}


TEST_CASE("Le constructeur sans surface fonctionne correctement") {

    point p1{1,1};
    point p2{2,2};
    surface * s=new standartSurface(1);
    brickBreakable b{p1,p2,4,s};

    SUBCASE("La brick possède les bonnes coordonnées") {
        REQUIRE_EQ(p1.getX(), b.getCornerTopLeft().getX());
        REQUIRE_EQ(p1.getY(), b.getCornerTopLeft().getY());

        REQUIRE_EQ(p2.getX(), b.getCornerBottomRight().getX());
        REQUIRE_EQ(p2.getY(), b.getCornerBottomRight().getY());
    }
    SUBCASE("La méthode getSurface retorune un pointeur sur sa surface") {
        REQUIRE_NE(b.getSurface(), nullptr);
    }
}
