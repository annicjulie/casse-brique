#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "../libraries/doctest.h"
#include "../wall.h"
#include "../point.h"
#include "../standartSurface.h"

inline void lePointTopLeftEstExactement(wall* w, int x, int y)
{
    REQUIRE_EQ(w->getCornerTopLeft().getX(), x);
    REQUIRE_EQ(w->getCornerTopLeft().getY(), y);
}

inline void lePointBottomRightEstExactement(wall* w, int x, int y)
{
    REQUIRE_EQ(w->getCornerBottomRight().getX(), x);
    REQUIRE_EQ(w->getCornerBottomRight().getY(), y);
}

inline void laSurfaceEstExactement(wall* w, float speedCoef)
{
    REQUIRE_EQ(w->getSurface().getSpeedCoef(), speedCoef);
}


TEST_CASE("Les constructeurs de la classe wall sont corrects")
{
    SUBCASE("Le constructeur par défault fonctionne correctement")
    {
        wall* w = new wall{};
        lePointTopLeftEstExactement(w, 0, 0);
        lePointBottomRightEstExactement(w, 0, 0);
        laSurfaceEstExactement(w, 1.0);
    }
    SUBCASE("Le constructeur avec le point fonctionne correctement")
    {
        int x1 = 2;
        int y1 = 3;
        int x2 = 4;
        int y2 = 5;
        point p1{ x1,y1 };
        point p2{ x2,y2 };
        standartSurface s{1.0};
        wall* w = new wall{ p1,p2};
        s.setObjetSurfacique(w);
        lePointTopLeftEstExactement(w, 2, 3);
        lePointBottomRightEstExactement(w, 4, 5);
        laSurfaceEstExactement(w, 1.0);
    }
}

TEST_CASE("Les fonctions de la classe wall fonctionnent correctement")
{
    SUBCASE("La fonction setCornerTopLeft avec le point fonctionne correctement")
    {
        wall* w = new wall{};
        point p1{ 2,3 };
        w->setCornerTopLeft(p1);
        lePointTopLeftEstExactement(w, 2, 3);
        laSurfaceEstExactement(w, 1.0);
    }
    SUBCASE("La fonction setCornerTopLeft avec les valeurs fonctionne correctement")
    {
        wall* w = new wall{};
        int x = 5;
        int y = 10;
        w->setCornerTopLeft(x, y);
        lePointTopLeftEstExactement(w, 5, 10);
        laSurfaceEstExactement(w, 1.0);
    }
    SUBCASE("La fonction setCornerBottomRight avec le point fonctionne correctement")
    {
        wall* w = new wall{};
        point p1{ 4,5 };
        w->setCornerBottomRight(p1);
        lePointBottomRightEstExactement(w, 4, 5);
        laSurfaceEstExactement(w, 1.0);
    }
    SUBCASE("La fonction setCornerBottomRight avec les valeurs fonctionne correctement")
    {
        wall* w = new wall{};
        int x = 5;
        int y = 10;
        w->setCornerBottomRight(x, y);
        lePointBottomRightEstExactement(w, 5, 10);
        laSurfaceEstExactement(w, 1.0);
    }
}