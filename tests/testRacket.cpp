#include "../libraries/doctest.h"
#include "../racket.h"
#include "../point.h"
#include "../standartSurface.h"

inline void lePointEstExactement(racket *r, int x, int y)
{
    REQUIRE( r->getCornerTopLeft().getX() == x );
    REQUIRE( r->getCornerTopLeft().getY() == y );
}

inline void laSurfaceEstExactement(racket *r, float speedCoef)
{
    REQUIRE_EQ( r->getSurface().getSpeedCoef(),speedCoef );
}


TEST_CASE("Les constructeurs de la classe racket sont corrects")
{
    SUBCASE("Le constructeur par d�fault fonctionne correctement")
    {
        racket *r = new racket{};
        lePointEstExactement(r,0,0);
        laSurfaceEstExactement(r,1.0);
    }
    SUBCASE("Le constructeur avec le point fonctionne correctement")
    {
        int x=2;
        int y=3;
        point p{x,y};
        standartSurface s{racket::SPEEDCOEF};
        racket *r= new racket{p,s};
        s.setObjetSurfacique(r);
        lePointEstExactement(r,2,3);
        laSurfaceEstExactement(r,1.0);
    }
    SUBCASE("Le constructeur avec des valeurs fonctionne correctement")
    {
        int x=2;
        int y=3;
        standartSurface s{racket::SPEEDCOEF};
        racket *r = new racket(x,y,s);
        lePointEstExactement(r,2,3);
        laSurfaceEstExactement(r,1.0);
    }
}

TEST_CASE("Les fonctions de la classe racket fonctionnent correctment")
{
    SUBCASE("La fonction setCornerTopLeft avec le point fonctionne correctement")
    {
        racket *r = new racket{};
        point p{2,3};
        r->setCornerTopLeft(p);
        lePointEstExactement(r,2,3);
        laSurfaceEstExactement(r,1.0);
    }
    SUBCASE("La fonction setCornerTopLeft avec les valeurs fonctionne correctement")
    {
        racket *r = new racket{};
        int x=5;
        int y=10;
        r->setCornerTopLeft(x,y);
        lePointEstExactement(r,5,10);
        laSurfaceEstExactement(r,1.0);
    }
    SUBCASE("La fonction moveLeft fonctionne correctement")
    {
        standartSurface s{racket::SPEEDCOEF};
        racket *r = new racket(10,20,s);
        r->moveLeft();
        REQUIRE( r->getCornerTopLeft().getX() == 5);
    }
    SUBCASE("La fonction moveRight fonctionne correctement")
    {
        standartSurface s{racket::SPEEDCOEF};
        racket *r = new racket(10,20,s);
        r->moveRight();
        REQUIRE( r->getCornerTopLeft().getX() == 15);
    }
}
