#include "../libraries/doctest.h"
#include "../ball.h"
#include "../point.h"
#include <cmath>

void lesCoordonnesDuCentreSontExactement(const ball& b,int x, int y)
{
    REQUIRE( b.getCenter().getX() == x );
    REQUIRE( b.getCenter().getY() == y );
}

void laVitesseEstExactement(const ball& b,float speed)
{
    REQUIRE( b.getSpeed() == speed );
}

void laDirectionEstExactement(const ball& b,float direction)
{
    REQUIRE( b.getDirection() == direction );
}

TEST_CASE("Les constructeurs de la classe ball sont corrects")
{
    SUBCASE("Le constructeur par d�fault fonctionne correctement")
    {
        ball b {};
        lesCoordonnesDuCentreSontExactement(b,0,0);
        laVitesseEstExactement(b,0.0);
        laDirectionEstExactement(b,0.0);
    }
    SUBCASE("Le constructeur avec le point fonctionne correctement")
    {
        float speed = 10.5;
        float direction = 3.2;
        int x=10;
        int y=20;
        point p{x,y};
        ball b {p,speed,direction};
        lesCoordonnesDuCentreSontExactement(b,x,y);
        laVitesseEstExactement(b,speed);
        laDirectionEstExactement(b,direction);
    }
    SUBCASE("Le constructeur avec les valeurs fonctionne correctement")
    {
        float speed = 10.5;
        float direction = 3.2;
        int x=10;
        int y=20;
        ball b {x,y,speed,direction};
        lesCoordonnesDuCentreSontExactement(b,x,y);
        laVitesseEstExactement(b,speed);
        laDirectionEstExactement(b,direction);
    }
}

TEST_CASE("Les fonctions set fonctionnent correctment")
{
    ball b{};
    SUBCASE("La fonction setSpeed fonctionne correctement")
    {
        b.setSpeed(2.6);
        laVitesseEstExactement(b,2.6);
    }
    SUBCASE("La fonction setDirection fonctionne correctement")
    {
        b.setDirection(5.3);
        laDirectionEstExactement(b,5.3);
    }
    SUBCASE("La fonction setCenter avec le point fonctionne correctement")
    {
        point p{1,2};
        b.setCenter(p);
        lesCoordonnesDuCentreSontExactement(b,1,2);
    }
    SUBCASE("La fonction setCenter avec les valeurs fonctionne correctement")
    {
        int x=5;
        int y=10;
        b.setCenter(x,y);
        lesCoordonnesDuCentreSontExactement(b,5,10);
    }
}

TEST_CASE("La fonction moveb fonctionne correctment")
{
    ball b{10,20,1.5,2.3};
    b.moveb();

    REQUIRE( b.getCenter().getX() == static_cast<int>(10 + (1.5*cos(2.3*(M_PI/180)))));
    REQUIRE( b.getCenter().getY() == static_cast<int>(20 - (1.5*sin(2.3*(M_PI/180)))));
}
