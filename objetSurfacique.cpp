#include "objetSurfacique.h"

objetSurfacique::~objetSurfacique()
{}

point objetSurfacique::getCornerTopLeft() const
{}

point objetSurfacique::getCornerBottomRight() const
{}

int objetSurfacique::returnHeight() const
{
    return( getCornerBottomRight().getY() - getCornerTopLeft().getY() );
}

int objetSurfacique::returnLength() const
{
    return (getCornerBottomRight().getX() - getCornerTopLeft().getX() );
}

int objetSurfacique::topLeftCoordsX() const
{
    return getCornerTopLeft().getX();
}

int objetSurfacique::topLeftCoordsY() const
{
    return getCornerTopLeft().getY();
}

int objetSurfacique::bottomRightCoordsX() const
{
    return getCornerBottomRight().getX();
}

int objetSurfacique::bottomRightCoordsY() const
{
    return getCornerBottomRight().getY();
}
