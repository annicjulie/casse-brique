#include "brick.h"
#include "point.h"

brick::brick(point cornerTopLeft, point cornerBottomRight): d_cornerTopLeft{cornerTopLeft}, d_cornerBottomRight{cornerBottomRight}
{}

brick::brick(point cornerTopLeft, point cornerBottomRight, surface *surface): d_cornerTopLeft{cornerTopLeft}, d_cornerBottomRight{cornerBottomRight}, d_surface{surface}
{}


point brick::getCornerTopLeft() const{return d_cornerTopLeft;}

void brick::setCornerTopLeft(point cornerTopLeft){
    d_cornerTopLeft=cornerTopLeft;
}

point brick::getCornerBottomRight() const{return d_cornerBottomRight;}


void brick::setCornerBottomRight(point cornerBottomRight){
    d_cornerBottomRight=cornerBottomRight;
}

surface* brick::getSurface() const{return d_surface;}


void brick::setSurface(surface* surface){
    d_surface=surface;
}

int brick::getState() const{return -1;}

void brick::setState(int state){}

void brick::decreaseState(){}

bool brick::isDestroyed() const{
    return false;
}

bool brick::getIsKiller() const{

}

