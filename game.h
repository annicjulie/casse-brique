#ifndef GAME_H
#define GAME_H

// Exclude rarely-used stuff from Windows headers
#define WIN32_LEAN_AND_MEAN

// Windows Header Files:
#include <windows.h>

#include "ball.h"
#include "graphicalInterface.h"
#include "racket.h"
#include "wall.h"
#include "brickPattern.h"

class game {
public:
    game(string mapPath);
    void run();
    void initInterface();
    bool getGameOver();
    void setGameOver(bool state);

private:
    bool d_gameOver;
    ball d_ball;
    brickPattern d_brickPattern;
    graphicalInterface d_graphicalInterface;
    racket *d_racket;
    wall *d_topWall, *d_leftWall, *d_rightWall;
};

#endif // GAME_H
