#include "wall.h"

wall::wall() : d_cornerTopLeft{}, d_cornerBottomRight{}, d_wallSurface{ standartSurface{1} }
{}

wall::wall(const point &TopLeft, const point& BottomRight) : d_cornerTopLeft{TopLeft}, d_cornerBottomRight{BottomRight}, d_wallSurface{standartSurface{1}}
{}

wall::~wall()
{}

point wall::getCornerBottomRight() const 
{
    return d_cornerBottomRight;
}

point wall::getCornerTopLeft() const
{
    return d_cornerTopLeft;
}

void wall::setCornerTopLeft(const point& p)
{
    d_cornerTopLeft.setXY(p.getX(), p.getY());
}

void wall::setCornerTopLeft(int x, int y)
{
    d_cornerTopLeft.setXY(x, y);
}

void wall::setCornerBottomRight(const point& p)
{
    d_cornerBottomRight.setXY(p.getX(), p.getY());
}

void wall::setCornerBottomRight(int x, int y)
{
    d_cornerBottomRight.setXY(x, y);
}

standartSurface wall::getSurface()
{
    return d_wallSurface;
}
