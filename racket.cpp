#include "racket.h"

racket::racket() : d_cornerTopLeft{}, d_surface{standartSurface{SPEEDCOEF}}
{}

racket::racket(point &topLeft) : d_cornerTopLeft{topLeft}, d_surface{standartSurface(SPEEDCOEF)}
{}

racket::racket(int x, int y) : d_cornerTopLeft{x,y}, d_surface{standartSurface(SPEEDCOEF)}
{}

void racket::setCornerTopLeft(const point &p)
{
    d_cornerTopLeft.setXY(p.getX(),p.getY());
}

void racket::setCornerTopLeft(int x, int y)
{
    d_cornerTopLeft.setXY(x,y);
}

point racket::getCornerTopLeft() const
{
    return d_cornerTopLeft;
}

point racket::getCornerBottomRight() const
{
    return point{d_cornerTopLeft.getX()+ WIDTH , d_cornerTopLeft.getY()+ HEIGHT};
}

standartSurface racket::getSurface() const
{
    return d_surface;
}

void racket::moveLeft()
{
    d_cornerTopLeft.setX(d_cornerTopLeft.getX()-5);
}

void racket::moveRight()
{
    d_cornerTopLeft.setX(d_cornerTopLeft.getX()+5);
}
